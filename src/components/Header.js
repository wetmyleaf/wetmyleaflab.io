import React from 'react';

export default ({ children }) => (
  <header className="header">
    <div className="logo">
      <h1 className="ta-center">Wet My Leaf</h1>
    </div>
  </header>
);
